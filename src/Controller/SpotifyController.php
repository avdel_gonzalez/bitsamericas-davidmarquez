<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use SpotifyWebAPI;

class SpotifyController extends AbstractController
{

    /**
     * @Route("/", name="index")
     */
    public function index(){
        return $this->render('spotify/index.html.twig');
    }

    /**
     * @Route("/releases", name="releases")
     */
    public function releases(): Response
    {

        $session = new SpotifyWebAPI\Session(
            $this->getParameter('app.spotifyClientId'),
            $this->getParameter('app.spotifyClientSecret')
        );

        $session->requestCredentialsToken();
        $accessToken = $session->getAccessToken();

        $api = new SpotifyWebAPI\SpotifyWebAPI();
        $api->setAccessToken($accessToken);

        $releases = $api->getNewReleases([
            'country' => 'co',
        ]);

        return $this->render('spotify/releases.html.twig', [
            'releases' => $releases->albums->items,
        ]);
    }

    /**
     * @Route("/artist/{id}", name="artist")
     */
    public function artist($id){
        $session = new SpotifyWebAPI\Session(
            $this->getParameter('app.spotifyClientId'),
            $this->getParameter('app.spotifyClientSecret')
        );

        $session->requestCredentialsToken();
        $accessToken = $session->getAccessToken();

        $api = new SpotifyWebAPI\SpotifyWebAPI();
        $api->setAccessToken($accessToken);

        $artist = $api->getArtist($id);
        $albums = $api->getArtistAlbums($id);

        return $this->render('spotify/artist.html.twig', [
            'artist' => $artist,
            'albums' => $albums->items
        ]);
    }
}
