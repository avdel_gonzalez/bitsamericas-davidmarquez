Prueba de integración Spotify<br />
David Felipe Márquez González<br /><br />

<strong>Especificaciones</strong><br />
Symfony 5.4.2<br />
Bootstrap 4 Template<br /><br />

<strong>Instalación</strong><br />
git clone https://gitlab.com/avdel_gonzalez/bitsamericas-davidmarquez.git<br />
composer install<br />
symfony server:start<br /><br />

<strong>##> Spotify Parameters ##</strong><br />
SPOTIFY_CLIENT_ID="f6cc446b4e854e37a295f267def9286d"<br />
SPOTIFY_CLIENT_SECRET="4eeb7a070c5d4666bc630bea8c283e7e"<br />